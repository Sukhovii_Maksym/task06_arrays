package deque;

public class AppTryMyDeque {
    public static void main(String[] args) {
        Deque deque=new Deque();
        deque.addFirst("aaa");
        deque.addFirst("bbb");
        deque.addLast("ccc");
        deque.addLast("ddd");
        deque.print();
        System.out.println("\nAfter remove:");
        deque.removeFirst();
        deque.removeLast();
        deque.print();
    }
}
