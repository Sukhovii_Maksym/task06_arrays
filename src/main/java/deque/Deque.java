package deque;

//Realisation with two arrays
public class Deque {
    private Object[] tail =new Object[0];
    private Object[] head =new Object[0];

    private Object[] getTail() {
        return tail;
    }

    private void setTail(Object[] tail) {
        this.tail = tail;
    }

    private Object[] getHead() {
        return head;
    }

    private void setHead(Object[] head) {
        this.head = head;
    }

    public Deque() {
    }
    public void addFirst(Object o){
        Object[] arr = new Object[head.length+1];
        for (int i=0;i<head.length;i++){
            arr[i] = head[i];
        }
        arr[arr.length-1]=o;
        setHead(arr);
    }
    public void addLast(Object o){
        Object[] arr = new Object[tail.length+1];
        for (int i=0;i<tail.length;i++){
            arr[i] = tail[i];
        }
        arr[arr.length-1]=o;
        setTail(arr);
    }
    public Object getFirst(){
        return head[head.length-1];
    }
    public Object getLast(){
        return tail[tail.length-1];
    }

    public void removeFirst(){
            Object[] arr = new Object[head.length-1];
            for (int i=0;i<arr.length;i++){
                arr[i] = head[i];
            }
            setHead(arr);
    }
    public void removeLast(){
        Object[] arr = new Object[tail.length-1];
        for (int i=0;i<arr.length;i++){
            arr[i] = tail[i];
        }
        setTail(arr);
    }
    public void print(){
        System.out.println("Deque: ");
        for(int i=head.length-1;i>=0;i--)
            System.out.print(head[i]+"; ");
        for(int j=0;j<tail.length;j++)
            System.out.print(tail[j]+"; ");
            }
}
