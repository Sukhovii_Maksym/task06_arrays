package generic;

import java.util.ArrayList;
import java.util.List;

public class LetterBox <T> {
    List<T> list =new ArrayList();

    public LetterBox() {
    }

    public LetterBox(List<T> list) {
        this.list = list;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
    public void add(T t){
        list.add(t);
    }
}
