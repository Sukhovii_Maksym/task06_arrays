package generic;
//lines with compiler error is commented

public class GenericContainer {
    public static void main(String[] args) {
        Letter letter = new Letter(1,"Moon 1","Hello to everyBody");
        Postcard postcard = new Postcard(1,"Earth","Hello humans",new Picture());
        Object object=new Object();

        LetterBox <Letter> letterBox1 = new LetterBox();
        letterBox1.add(letter);
        letterBox1.add(postcard);
        letterBox1.add(null);


        LetterBox <? extends Letter> letterBox2 = new LetterBox();
//        letterBox2.add(letter);
//        letterBox2.add(postcard);
//        letterBox2.add(object);


        LetterBox <? super Letter> letterBox3 = new LetterBox();
        letterBox3.add(letter);
        letterBox3.add(postcard);
//        letterBox3.add(object);

        //--------------------------------------------------------//

        LetterBox <Postcard> letterBox4 = new LetterBox();
//        letterBox4.add(letter);
        letterBox4.add(postcard);
        letterBox4.add(null);


        LetterBox <? extends Postcard> letterBox5 = new LetterBox();
//        letterBox5.add(letter);
//        letterBox5.add(postcard);
//        letterBox5.add(object);


        LetterBox <? super Postcard> letterBox6 = new LetterBox();
//        letterBox6.add(letter);
        letterBox6.add(postcard);
//        letterBox6.add(object);
    }



}
