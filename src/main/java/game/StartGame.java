package game;

public class StartGame {
    public static void main(String[] args) {
        HeroInTheHall hero= new HeroInTheHall("Petro");
        hero.printWhatIsBehindTheDoor();
        hero.howManyMortalMonsters();
        hero.rightStrategy();
    }
}
