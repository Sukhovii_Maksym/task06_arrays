package priorityQueue;

public class Queue <T>{
    private  T[] arr;
    private int size;
    private int indexOfLastElement=-1;

    public Queue() {
        this.size = 10; //default size
        this.arr = (T[]) new Object[size];

    }

    public Queue(int size) {
        this.size = size;
        this.arr = (T[]) new Object[size];    }

    public T[] getArr() {
        return arr;
    }

    public void setArr(T[] arr) {
        this.arr = arr;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    public T get(int index){
        return arr[index];
    }
    public void add(T t){
        if (indexOfLastElement<size){
            arr[++indexOfLastElement]=t;
        }
        else{
            this.size *=2;
            T[] buffer = (T[]) new Object[size];
            for(int i=0;i<indexOfLastElement;i++){
                buffer[i] = arr[i];
            }
            buffer[indexOfLastElement]=t;
            this.arr=buffer;
        }

    }

    public boolean delete(int index){
            if (index >= 0&index<=indexOfLastElement){
                for(int i=index;i<indexOfLastElement;i++){
                    arr[i]=arr[i+1];
                }
                arr[indexOfLastElement]=null;
                indexOfLastElement--;
                return true;
            }
            else return false;
    }
    public boolean delete(){
        if(indexOfLastElement==0)
        arr[indexOfLastElement]=null;
        else {
            arr[indexOfLastElement]=null;
            indexOfLastElement--;

        }
        return true;
    }
    public void print(){
        for (int i=0; i<=indexOfLastElement;i++){
            System.out.println(i+"--"+arr[i]);
        }
    }
}
