package priorityQueue;

public class Application {
    public static void main(String[] args) {
        PriorityQueue <String> priorityQueue = new PriorityQueue<String>(8);
        priorityQueue.add("aaa");
        priorityQueue.add("bbb");
        priorityQueue.add("ccc");
        priorityQueue.print();
        priorityQueue.delete(1);
        priorityQueue.print();
    }
}
