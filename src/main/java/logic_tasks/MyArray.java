package logic_tasks;


import java.lang.reflect.Array;

public class MyArray {
    private Object[] arr;

    public MyArray(Object[] arr) {
        this.arr = arr;
    }

    public MyArray() {

    }

    public Object[] getArr() {
        return arr;
    }

    public void setArr(Object[] arr) {
        this.arr = arr;
    }

    //----------------------------------
    public void print() {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "; ");
        }
        System.out.println("\n");
    }

    //----------------------------------
    public boolean isHere(Object t) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(t)) {
                return true;
            }

        }
        return false;
    }

    //----------------------------------
    public MyArray intersection(MyArray myArrayT) {
        Object[] arrT = myArrayT.getArr();
        Object[] intersection = new Object[Math.min(arrT.length, arr.length)];
        int k = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arrT.length; j++) {
                if (arr[i].equals(arrT[j])) {
                    intersection[k++] = arr[i];
                }
            }

        }
        Object[] cutedIntersection = new Object[k];
        for (int i = 0; i < k; i++) {
            cutedIntersection[i] = intersection[i];
        }

        return new MyArray(cutedIntersection);
    }

    //---------------------------------------
    public void remove(int index) {
        Object[] arrBuf = new Object[arr.length - 1];
        for (int i = 0; i < arrBuf.length; i++) {
            if (i < index) {
                arrBuf[i] = arr[i];
            } else
                arrBuf[i] = arr[i + 1];
        }
        arr = arrBuf;
    }

    //---------------------------------------
    //Task B - delete all value more then count = 2
    public MyArray deleteThird() {

        MyArray myArray = new MyArray(arr);
        for (int i = 0; i < myArray.getArr().length; i++) {
                int count = 0;
                int k =i;
                    while( k<myArray.getArr().length-1) {
                        k++;
                        if (myArray.getArr()[k].equals(myArray.getArr()[i])) {
                            if (count>0) {
                                myArray.remove(k);
                                k--;
                            }
                            count++;
                        }
                    }
                    }



        return myArray;
    }

    //---------------------------------------------------
    //Task C delete other
    public MyArray withoutTwink() {
        MyArray myArray = new MyArray(arr);
        int i=0;
        while(i<myArray.getArr().length-1) {

            if (myArray.getArr()[i].equals(myArray.getArr()[i + 1])) {
                myArray.remove( 1+i--);
            }
        i++;
        }
        return myArray;
    }

}
