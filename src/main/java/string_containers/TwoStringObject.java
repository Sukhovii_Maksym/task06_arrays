package string_containers;

public class TwoStringObject implements Comparable<TwoStringObject> {
    private String first;
    private String second;


    public int compareTo(TwoStringObject tso) {
        return first.compareTo(tso.first);
    }
    public int compareToSecond(TwoStringObject tso) {
        return second.compareTo(tso.second);
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }
    public void set(String first,String second){
        this.first = first;
        this.second=second;
    }

    @Override
    public String toString() {
        return "TwoStringObject{" +
                "first='" + first + '\'' +
                ", second='" + second + '\'' +
                '}';
    }
}
