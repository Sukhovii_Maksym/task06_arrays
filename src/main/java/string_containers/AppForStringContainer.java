package string_containers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class AppForStringContainer {
    public static void main(String[] args) {
        StringContainer sc = new StringContainer();
        sc.add("One");
        sc.add("Two");
        sc.add("Three");
        sc.print();
        sc.delete(1);
        sc.print();
        //------------------------------------------
        System.out.println("\n//-------object two strings------//");
        TwoStringObject tso1 = new TwoStringObject();
        TwoStringObject tso2 = new TwoStringObject();
        TwoStringObject tso3 = new TwoStringObject();
        tso1.set("bbb", "888");
        tso2.set("aaa", "999");
        tso3.set("ccc", "777");
        TwoStringObject[] tsoArray = {tso1, tso2, tso3};
        ArrayList<TwoStringObject> tsoArrayList = new ArrayList<TwoStringObject>();
        tsoArrayList.add(tso1);
        tsoArrayList.add(tso2);
        tsoArrayList.add(tso3);
        Arrays.sort(tsoArray, TwoStringObject::compareToSecond);
        Arrays.stream(tsoArray).forEach(System.out::println);
        System.out.println("------------------");
        tsoArrayList.forEach(System.out::println);
        System.out.println("------------------");
        tsoArrayList.sort(TwoStringObject::compareTo);
        tsoArrayList.forEach(System.out::println);
    }
}
